h3n2
====
```
File:   h3n2.fa  
RefSeq: GCF_000865085.1  
Link:   http://www.ncbi.nlm.nih.gov/assembly/GCF_000865085.1/  

Files:  SRR1928148_?.fq.gz  
Source: PRJNA278105  
DOI:    10.1016/j.jcv.2015.04.010  
Link:   http://www.ncbi.nlm.nih.gov/sra/?term=SRR1928148  
```
